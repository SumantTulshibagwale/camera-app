import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:math' as Math;
import 'package:camera/camera.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';

List<CameraDescription> cameras;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  cameras = await availableCameras();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: CamOverlay(),
    );
  }
}

class CamOverlay extends StatefulWidget {
  @override
  _CamOverlayState createState() => _CamOverlayState();
}

class _CamOverlayState extends State<CamOverlay> {
  CameraController _controller;

  bool showImage = false;
  String path = '';

  @override
  void initState() {
    super.initState();

    _controller = CameraController(cameras[0], ResolutionPreset.medium);
    _controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Camera Overylay"),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        onPressed: () async {
          final storagePath = join(
            // Store the picture in the temp directory.
            // Find the temp directory using the `path_provider` plugin.
            (await getApplicationDocumentsDirectory()).path,
            '${DateTime.now()}.png',
          );
          await _controller.takePicture(storagePath);
          setState(() {
            showImage = true;
            path = storagePath;
          });
        },
      ),
      body: showImage
          ? Image.file(File(path))
          : Container(
              child: Stack(
              alignment: FractionalOffset.center,
              children: <Widget>[
                new Positioned.fill(
                  child: !_controller.value.isInitialized
                      ? Container()
                      : new AspectRatio(
                          aspectRatio: _controller.value.aspectRatio,
                          child: new CameraPreview(_controller)),
                ),
                new Positioned.fill(
                  child: new Opacity(
                      opacity: 1,
                      child: Container(
                        height: 200.0,
                        child: Center(
                          child: Transform.rotate(
                            angle: Math.pi / 180 * 90,
                            alignment: Alignment.center,
                            child: Container(
                              height: MediaQuery.of(context).size.height - 130,
                              alignment: Alignment.center,
                              child: Image.asset('assets/overlay.png'),
                            ),
                          ),
                        ),
                      )),
                ),
              ],
            )),
    );
  }
}
